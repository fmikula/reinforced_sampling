
## Packages

pkgs = ["BSON",
"Dates",
"DelimitedFiles",
"FFTW",
"FileIO",
"HCubature",
"HDF5",
"ImageMagick",
"Images",
"InteractiveUtils",
"JSON",
"LinearAlgebra",
"MAT",
"MLDatasets",
"MultivariateStats",
"Plots",
"Profile",
"ProgressMeter",
"PyPlot",
"SparseArrays"]


using Pkg

Pkg.add(pkgs)
