using LinearAlgebra
using DelimitedFiles: readdlm, writedlm
using Plots
using MAT
using Statistics
using HCubature

function split_images(X::Array{Float64, 3}, split_length::Int)
    n = size(X,1)
    image_slX = size(X,2)
    image_slY = size(X,3)

    sl = split_length
    dim = sl^2
    splitsX = div(image_slX, sl)
    splitsY = div(image_slY, sl)
    temp = zeros(n * splitsX * splitsY, dim)

    i = 0
    for ind_im in 1:n
        arr = X[ind_im, :, :]
        for sx in 1:splitsX
            for sy in 1:splitsY
                i += 1
                cut = arr[sl*(sx-1)+1:sl*sx, sl*(sy-1)+1:sl*sy]
                temp[i,:] = reshape(cut, :)
            end
        end
    end
    return temp
end

function transform_to_retina(X::Array{Float64}, W=nothing; scale::Float64 = 0.7)
    if isnothing(W)
        n = size(X,2)
        l = Int(sqrt(n))
        W = zeros(n,n)

        folder = "../cell_locations/"
        filename = "W$(n)_$(l)_$(scale).csv"

        if filename in readdir(folder)
            W = readdlm(folder * filename)
        else
            # coordinates of pixel centers from -0.5 to l-1.5
            xs = reshape(collect(Iterators.product(1:l, 1:l)), :)
            xs = map(x -> collect(x).-1.5, xs)

            # coordinates of n gc centers from 0 to l-2
            rs = get_ganglion_coordinates(n,l-2)
            # scatter(hcat(rs[:,1],xs[:,1]),hcat(rs[:,2],xs[:,2]))

            var1 = [scale, scale]
            var2 = 1.6 .* var1

            for i in 1:n
                r = rs[i,:]
                k = get_ganglion_field(r, xs, var1, var2)
                W[:,i] = k
            end

            # scale matrix so the transformation preserves the variance
            W = W ./ sqrt(var(X * W))
            writedlm(folder * filename, W)
        end
    end
    X = X * W
    return W, X
end

function get_ganglion_field(r::Array{Float64}, xs::Array{Array{Float64,1},1},
    var1::Array{Float64}, var2::Array{Float64})

    f(x) = multivariate_gaussian(convert(Array{Float64},x), r, var1) -
           multivariate_gaussian(convert(Array{Float64},x), r, var2)
    return [hcubature(f, x.-0.5, x.+0.5)[1] for x in xs]
end

function get_ganglion_coordinates(n::Int, l::Int)
    folder = "../cell_locations/"
    filename = "$(n)_$(l+2).csv"
    if filename in readdir(folder)
        r = readdlm(folder * filename)
    else
        r = rand(Float64,n,2) * l
        for t in 1:3000
            dr = zeros(n,2)
            for i in 1:n
                for j in 1:n
                    if i != j
                        q = r[i,:] - r[j,:]
                        dr[i,:] += q ./ max(eps(1.0),norm(q)^5)
                    end
                end
            end
            r += 0.0008 .* dr
            r = map(x -> max(rand()*0.01, min(l+rand()*0.01, x)), r)
        end
        writedlm(folder * filename, r)
    end
    return r
end

function multivariate_gaussian(x::Array{Float64}, mean::Array{Float64}, var::Array{Float64})
    n = length(mean)
    norm = 1.0/(sqrt(2*pi)^n * sqrt(prod(var)))
    M = x - mean
    exponent = exp(- 0.5 * M' * Diagonal(var.^(-1)) * M)
    return norm * exponent
end

function sigmoid(x::Float64)
    return 1 - 1.0 / (1.0 + exp(x))
end

function nonlinearity(x::Float64, shift::Float64, scale::Float64)
    return sigmoid(scale * (x - shift))
end

nlShift = 0.05
nlScale = 16.0
nPatterns = 10000
sl = 12
nImg = sl^2
n = nImg*2

dic = matread("./IMAGES.mat")
#dic = matread("data/scenes/IMAGES_RAW.mat")
imgs = dic["IMAGES"] # (512,512,10)
imgs = permutedims(imgs, [3, 1, 2]) # (10,512,512)

split_input = Array{Float64}(undef, 0, nImg)
for i in 0:3
    for j in 0:3
        # -8 as we leave away the border
        splitsX = div(512-8-i, sl)
        splitsY = div(512-8-j, sl)
        temp = split_images(imgs[:,5+i:splitsX*sl,5+j:splitsY*sl], sl)
        global split_input = cat(split_input, temp, dims=1)
    end
end

#W, white_split_input = transform_to_retina(split_input)
white_split_input = copy(split_input)

iput = zeros(nPatterns, n)

nl(x) = nonlinearity(x, nlShift, nlScale)
for i in 1:nPatterns
    ind = rand(1:size(white_split_input,1))
    temp = white_split_input[ind,:]
    img = vcat(temp, -temp)
    img = map(nl, img)
    global iput[i,:] .= img
end

histogram(reshape(split_input, :), normalize=true)
histogram!(reshape(white_split_input, :), normalize=true)
plot!(nl)
histogram(reshape(iput, :))
nl2(x) = nonlinearity(x, 0.7, 4.0)
histogram!(map(nl2,randn(nPatterns * n)))

#r = get_ganglion_coordinates(nImg,sl-2)
#heatmap(reshape(split_input[5,:],16,16))
#scatter(r[:,1],r[:,2],zcolor=white_split_input[5,:])

#p = zeros(nImg)
#p[50] = 0.4
#_, q = transform_to_retina(reshape(p,1,:), W)
#q = reshape(q,:)
#scatter(r[:,1],r[:,2],zcolor=map(nl,q))
#histogram(q)
