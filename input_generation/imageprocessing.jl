using MultivariateStats
using DelimitedFiles: readdlm, writedlm

""" Separates subimages in image with border for plotting"""
function separate_subimages(X::Array{Float64, 2}, sub_length::Int, border_width::Int)
    bw = border_width
    splits = split_images(X, sub_length)
    s = size(splits)
    X_new = zeros(s[1], s[2]+2*bw, s[3]+2*bw)
    X_new .= minimum(X)
    X_new[:,bw+1:end-bw,bw+1:end-bw] = splits
    return stitch_images(X_new)
end

""" Splits image into square tiles of a given side-length for single image"""
function split_images(X::Array{Float64, 2}, split_length::Int)
    image_slX = size(X,1)
    image_slY = size(X,2)

    sl = split_length
    splitsX = div(image_slX, sl)
    splitsY = div(image_slY, sl)
    temp = zeros(splitsX * splitsY, sl, sl)

    i = 0
    for sx in 1:splitsX
        for sy in 1:splitsY
            i += 1
            cut = X[sl*(sx-1)+1:sl*sx, sl*(sy-1)+1:sl*sy]
            temp[i,:,:] = cut
        end
    end
    return temp
end

""" Splits images into square tiles of a given side-length for a dataset"""
function split_images(X::Array{Float64, 3}, split_length::Int)
    n = size(X,1)
    image_slX = size(X,2)
    image_slY = size(X,3)

    sl = split_length
    dim = sl^2
    splitsX = div(image_slX, sl)
    splitsY = div(image_slY, sl)
    temp = zeros(n * splitsX * splitsY, dim)

    i = 0
    for ind_im in 1:n
        arr = X[ind_im, :, :]
        for sx in 1:splitsX
            for sy in 1:splitsY
                i += 1
                cut = arr[sl*(sx-1)+1:sl*sx, sl*(sy-1)+1:sl*sy]
                temp[i,:] = reshape(cut, :)
            end
        end
    end
    return temp
end

""" Takes the tiles of n (image_dim,image_dim) images and stiches them together"""
function stitch_images(X::Array{Float64,2}, n::Int, image_dim::Int, split_length::Int)
    m = size(X,1)
    cut_dim = size(X,2)

    sl = split_length
    splits = div(image_dim, dim)
    temp = zeros(n, image_dim, image_dim)

    i = 0
    for ind_im in 1:n
        for sx in 1:splits
            for sy in 1:splits
                i += 1
                cut = reshape(X[i, :], (sl, sl))
                temp[ind_im, sl*(sx-1)+1:sl*sx, sl*(sy-1)+1:sl*sy] = cut
            end
        end
    end
    return temp
end

function stitch_images(X::Array{Float64,3})
    m = Int(sqrt(size(X,1)))
    sl = size(X,2)

    image_dim = m * sl
    temp = zeros(image_dim, image_dim)

    i = 0
    for sx in 1:m
        for sy in 1:m
            i += 1
            cut = X[i, :, :]
            temp[sl*(sx-1)+1:sl*sx, sl*(sy-1)+1:sl*sy] = cut
        end
    end
    return temp
end

""" Whiten the images by using a 1/f artifical kernel
(See Olshausen 1997)"""
function whiten_images(X::Array{Float64,3}, f0::Int = 200)
    image_dim = size(X,2)
    out = copy(X)
    f = zeros(ComplexF64,image_dim,image_dim)
    for i in 1:image_dim
        for j in 1:image_dim
            r = norm([i,j] .- 0.5 .- div(image_dim,2))
            f[i,j] = r * exp(-(r/f0)^4)
        end
    end
    f = fftshift(f)
    whiten = real ∘ ifft ∘ (x->f .* x) ∘ fft
    for i in 1:size(X,1)
        out[i,:,:] = whiten(X[i,:,:])
    end
    return out
end

""" Whiten the images by using the empirical Power-spectrum"""
function whiten_images_exact(X::Array{Float64,3})
    image_dim = size(X,2)
    out = copy(X)

    # get average fourier transform
    temp = zeros(ComplexF64,image_dim,image_dim)
    for i in 1:size(X,1)
        temp += fftshift(fft(X[i,:,:]))
    end

    # get average over total frequency (distance from center)
    cor = zeros(ComplexF64,image_dim)
    count = zeros(Int64,image_dim)
    for i in 1:image_dim
        for j in 1:image_dim
            r = abs(norm([i,j] .- 0.5 .- div(image_dim,2)))
            cor[Int(round(r))] += abs(temp[i,j])
            count[Int(round(r))] += 1
        end
    end
    cor ./= map(x->max(1,x),count)

    # create inverse spectrum depending on distance (assume rotation symmetry)
    f = zeros(ComplexF64,image_dim,image_dim)
    for i in 1:image_dim
        for j in 1:image_dim
            r = abs(norm([i,j] .- 0.5 .- div(image_dim,2)))
            f[i,j] = (1.0 / cor[Int(round(r))])
        end
    end
    f = fftshift(f)

    # flatten spectrum of images
    whiten = real ∘ ifft ∘ (x->f .* x) ∘ fft
    for i in 1:size(X,1)
        out[i,:,:] = whiten(X[i,:,:])
    end
    return out
end

""" Whiten data using a whitening matrix computed with the Stats package.
 - W: Whitening matrix, if already calculated"""
function whiten_images_cholesky(X::Array{Float64,2}, W=nothing)
    X = permutedims(X, [2,1])
    if isnothing(W)
        f = fit(Whitening, X, mean=0)
        W = f.W
    end
    X = W' * X
    return W, permutedims(X, [2,1])
end

function invert_transform_cholesky(X::Array{Float64,2}, s::Dict{String,Any})
    nlShift = s["preprocessingNonlinearityShift"]::Float64
    nlScale = s["preprocessingNonlinearityScale"]::Float64
    splitPosNeg = s["splitPosNegInput"]::Bool
    W = s["whitening_transform"]

    temp = permutedims(X, [2,1])
    if splitPosNeg
        nImg = div(size(temp,1),2)
        inl(x) = inverse_nonlinearity(x, nlShift, nlScale)
        temp = map(inl, temp)
        temp = temp[1:nImg,:] - temp[nImg+1:end,:]
    end
    temp = inv(W') * temp
    return permutedims(temp, [2,1])
end

""" Transforms to basis of retinal ganglion cells.
 - W: Whitening matrix, if already calculated
 - scale: variance of positive (smaller) gaussian, i.e. the scale of the rec. field"""
 function transform_to_retina(X::Array{Float64}, W=nothing; scale::Float64 = 0.2)
     if isnothing(W)
         n = size(X,2)
         l = Int(sqrt(n))
         W = zeros(n,n)

         folder = "../input_generation/cell_locations/"
         filename = "W$(n)_$(l)_$(scale).csv"

         if filename in readdir(folder)
             W = readdlm(folder * filename)
         else
             # coordinates of pixel centers from -0.5 to l-1.5
             xs = reshape(collect(Iterators.product(1:l, 1:l)), :)
             xs = map(x -> collect(x).-1.5, xs)

             # coordinates of n gc centers from 0 to l-2
             rs = get_ganglion_coordinates(n,l-2)
             # scatter(hcat(rs[:,1],xs[:,1]),hcat(rs[:,2],xs[:,2]))

             var1 = [scale, scale]
             var2 = 1.6 .* var1

             for i in 1:n
                 r = rs[i,:]
                 k = get_ganglion_field(r, xs, var1, var2)
                 W[:,i] = k
             end

             # scale matrix so the transformation preserves the variance
             W = W ./ sqrt(var(X * W))
             writedlm(folder * filename, W)
         end
     end
     # X[#data,#kernel] = sum j, 1 to n: X[d,j] * W[j,k]
     X = X * W
     return W, X
 end

function get_ganglion_field(r::Array{Float64}, xs::Array{Array{Float64,1},1},
    var1::Array{Float64}, var2::Array{Float64})

    f(x) = multivariate_gaussian(convert(Array{Float64},x), r, var1) -
           multivariate_gaussian(convert(Array{Float64},x), r, var2)
    return [hcubature(f, x.-0.5, x.+0.5)[1] for x in xs]
end

function get_ganglion_coordinates(n::Int, l::Int)
    folder = "../input_generation/cell_locations/"
    filename = "$(n)_$(l+2).csv"
    if filename in readdir(folder)
        r = readdlm(folder * filename)
    else
        r = rand(Float64,n,2) * l
        for t in 1:3000
            dr = zeros(n,2)
            for i in 1:n
                for j in 1:n
                    if i != j
                        q = r[i,:] - r[j,:]
                        dr[i,:] += q ./ max(eps(1.0),norm(q)^5)
                    end
                end
            end
            r += 0.0008 .* dr
            r = map(x -> max(rand()*0.01, min(l+rand()*0.01, x)), r)
        end
        writedlm(folder * filename, r)
    end
    return r
end
