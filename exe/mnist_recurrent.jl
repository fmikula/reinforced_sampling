include("../src/settings.jl")
include("../src/main.jl")

function main_mnist(plotflag, s)
    print("Setting up...\n")
    nPatterns = 1000000
    nTestPatterns = 1000
    nNumbers = 3
    nPixel = 16

    set!(s, "comment", "recurrent_prior")
    set!(s, "showProgressBar", true)

    set!(s, "lengthEligibility", 2.0)
    set!(s, "dt", 1.0)
    set!(s, "tempLogSampleInterval", 5)
    set!(s, "snapshotLogInterval", 5)

    l = s["presentationLength"]
    nSteps = nPatterns * l
    set!(s, "tempLogInterval", 10000 * l)

    set!(s, "n_x", nPixel^2 )# number x neurons
    set!(s, "n_z", 9) # number z neurons

    set!(s, "learningRateFeedForward", 1.5e-5)
    set!(s, "learningRateInhibitoryRecurrent", 1e-5) 
    set!(s, "learningRateBias", 1e-5)
    set!(s, "learningRateDecoder", 7e-5)
    set!(s, "learningRateDecoderRecurrent", 6e-6)

    set!(s, "initialSigma", sqrt(0.2))
    set!(s, "initialDeltaU", 1.0)
    set!(s, "weightMean", 0.01)
    set!(s, "weightVariance", 0.0001)

    set!(s, "learnedSigma", false)

    set!(s, "rho", 0.015)

    inputs = create_mnist_inputs(nPatterns, s, nNumbers=nNumbers, scale=nPixel/28)
    test_inputs = create_mnist_test(nTestPatterns, s, nNumbers=nNumbers, scale=nPixel/28)
    test_times = collect(0:div(nSteps, 5):nSteps)

    return main("mnist", plotflag, s, nSteps, inputs, test_inputs, test_times)
end
    
plotflag = true
main_mnist(plotflag, copy(standardSettings))
