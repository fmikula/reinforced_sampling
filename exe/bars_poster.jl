include("../src/settings.jl")
include("../src/main.jl")

using Statistics: var
using DelimitedFiles
using Random

# streched time by factor 10

function main_bars(plotflag, s, corr, noise)
    print("Setting up...\n")
    nPatterns = 1000000
    nTestPatterns = 500
    
    set!(s, "showProgressBar", true)
    
    dt = 0.2
    set!(s, "dt", dt)
    set!(s, "tempLogSampleInterval", Int(5.0/dt))
    set!(s, "updateInterval", 1) 
    set!(s, "snapshotLogInterval", Int(1.0/dt))

    l = s["presentationLength"]
    nSteps = nPatterns * l
    set!(s, "tempLogInterval", 50000 * l)

    set!(s, "n_x", 4^2) # number x neurons
    set!(s, "n_z", 8) # number z neurons

    set!(s, "learningRateFeedForward", 2.0e-5)
    set!(s, "learningRateDecoder", 2.0e-5)
    set!(s, "learningRateInhibitoryRecurrent", 4.0e-5)
    set!(s, "learningRateHomeostaticBias", 1.0e-2)
    set!(s, "learningRateSigma", 7.0e-8)
    

    set!(s, "learnedSigma", true)
    set!(s, "initialSigma", 1.0)
    set!(s, "fixedFinalSigma", true)
    set!(s, "fixedFinalSigmaValue", sqrt(0.1))

    set!(s, "localLearning_xz", false)
    set!(s, "reparametrizeBias", true)
    set!(s, "learnedInhibition", false)
    set!(s, "homeostaticBiases", true)
    set!(s, "rho", 0.015)

    set!(s, "inputNoise", noise)

    inputs, _ = create_bar_inputs(nPatterns, s, corr, 0.0)
    test_inputs, labels = create_bar_inputs(nTestPatterns, s, corr, 0.0)
    test_times = [1, div(nSteps,2), nSteps-1]

    return main("bars", plotflag, s, nSteps, inputs, test_inputs, test_times, name="$(corr)")
end

plotflag = true
corr = 0.7
noise = 0.0
net = main_bars(plotflag, copy(standardSettings), corr, noise)
