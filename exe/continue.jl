include("../src/main.jl")

using BSON
using Random

@assert (length(ARGS) > 0) "Please provide the name of the net you want to run."

file = ARGS[1]
runfolder = join(split(file,"/")[1:end-1],"/")

dict = BSON.load("$(file)")
net = dict[:net]


println("Setting up...")
nPatterns = 200000
nTestPatterns = 500
plotflag = true

s = net.log.settings
l = s["presentationLength"]
nSteps = nPatterns * l
imageDim = div(net.n, 2)
interval = net.log.settings["tempLogInterval"]

for (i,array) in net.log.temp
    net.log.temp[i] = vcat(array, zeros(div(nSteps, interval), size(array,2)))
end

seed = 1235
Random.seed!(seed)
inputs = get_natural_scenes(nPatterns, net.log.settings, imageDim)
seed = 1234
Random.seed!(seed)
test_inputs = get_natural_scenes(nTestPatterns, net.log.settings, imageDim)
    
test_times = [i for i in 0:div(nSteps, 5):nSteps]

println("Running $(file)")

run_net(net, inputs, test_inputs, test_times, runfolder, s)

print("Saving log...\n")
write_status(runfolder, net, net.log)

if (plotflag)
    try
        print("Plotting...\n")
        folder = runfolder * "/plots"
        rm(folder, recursive=true, force=true)
        mkpath(folder)
        plot_patterns(test_inputs, folder, s)
        plot_net(net, folder)
    catch e
        print("Error while plotting:\n")
        bt = catch_backtrace()
        msg = sprint(showerror, e, bt)
        println(msg)
    end
end

print("Done.\n")
