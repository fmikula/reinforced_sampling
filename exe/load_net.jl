include("../src/log.jl")
include("../src/neuron.jl")
include("../src/net.jl")
include("../src/snapshot.jl")
include("../src/utils.jl")
include("../plot/plot_functions.jl")
include("../input_generation/imageprocessing.jl")

using BSON

function load(name::String)
    print("Loading " * name * "\n")
    dict = BSON.load(name)
    return dict[:net]
end
