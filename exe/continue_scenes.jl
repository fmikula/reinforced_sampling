include("../src/main.jl")

using BSON
using Random

ind = parse(Int,ARGS[1])

homedir = "/data.nst/fabmik/dendritic_balance/scenes_local_rate_scan/logs/"
files = readdir(homedir)

runfolder = homedir * files[ind]
file = runfolder * "/net.bson"

dict = BSON.load("$(file)")
net = dict[:net]

println("Setting up...")
nPatterns = 500000
nTestPatterns = 500
scale = 0.5
plotflag = true

s = net.log.settings
l = s["presentationLength"]
nSteps = nPatterns * l
imageDim = Int(sqrt(div(net.n, 2)))
interval = net.log.settings["tempLogInterval"]

for (i,array) in net.log.temp
    net.log.temp[i] = vcat(array, zeros(div(nSteps, interval), size(array,2)))
end

seed = 1235
Random.seed!(seed)
inputs = get_natural_scenes(nPatterns, net.log.settings, imageDim, scale=scale)
seed = 1234
Random.seed!(seed)
test_inputs = get_natural_scenes(nTestPatterns, net.log.settings, imageDim, scale=scale)
    
test_times = [i for i in 0:div(nSteps, 5):nSteps]

println("Running $(file)")

run_net(net, inputs, test_inputs, test_times, runfolder, s)

print("Saving log...\n")
write_status(runfolder, net, net.log)

if (plotflag)
    try
        print("Plotting...\n")
        folder = runfolder * "/plots"
        rm(folder, recursive=true, force=true)
        mkpath(folder)
        plot_patterns(test_inputs, folder, s)
        plot_net(net, folder)
    catch e
        print("Error while plotting:\n")
        bt = catch_backtrace()
        msg = sprint(showerror, e, bt)
        println(msg)
    end
end

print("Done.\n")
