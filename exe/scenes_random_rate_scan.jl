include("../src/settings.jl")
include("../src/main.jl")

using Profile
import Random

function shuffle!(W::Array{Float64,2})
    n1 = size(W,1)
    n2 = size(W,2)
    for i in 1:100*length(W)
        j = rand(1:n1)
        k = rand(1:n2)
        l = rand(1:n1)
        m = rand(1:n2)
        temp = W[j,k]
        W[j,k] = W[l,m]
        W[l,m] = temp
    end
end

function main_scenes(plotflag, s, m::Int64, rate::Float64, xz_weights::Array{Float64,2}, seed=1234)
    print("Setting up...\n")
    nPatterns = 200000
    nTestPatterns = 500
    imageDim = 8
    scale = 0.5

    dt = 0.2
    set!(s, "comment", "poprate=$(m*rate)_m=$(m)_rate=$(rate)")

    set!(s, "dt", dt)
    set!(s, "tempLogSampleInterval", Int(5.0/dt))
    set!(s, "updateInterval", Int(round(0.2/dt)))
    set!(s, "snapshotLogInterval", Int(3.0/dt))

    # mandatory for scenes
    set!(s, "splitPosNegInput", true)

    l = s["presentationLength"]
    nSteps = nPatterns * l
    set!(s, "tempLogInterval", 4000 * l)

    set!(s, "n_x", 2*imageDim^2) # number x neurons
    set!(s, "n_z", m) # number z neurons
    set!(s, "weightVariance", 0.006)
    set!(s, "weightMean", 0.02)

    set!(s, "learnedSigma", true)
    set!(s, "initialSigma", 1.0)
    set!(s, "fixedFinalSigma", true)
    set!(s, "fixedFinalSigmaValue", sqrt(0.13))    

    set!(s, "localLearning_xz", false)
    set!(s, "reparametrizeBias", false)
    set!(s, "learnedInhibition", true)
    set!(s, "homeostaticBiases", true)

    set!(s, "rho", rate)

    set!(s, "learningRateSigma", 5e-7)

    s["paramChangeDict"]["learningRateDecoder"] =
        Dict(1          => 4e-5)
    s["paramChangeDict"]["learningRateFeedForward"] =
        Dict(1          => 0.0)
    s["paramChangeDict"]["learningRateHomeostaticBias"] =
        Dict(1          => 9e-4)
    s["paramChangeDict"]["learningRateInhibitoryRecurrent"] =
        Dict(1          => 4e-5)

    Random.seed!(seed)
    inputs = get_natural_scenes(nPatterns, s, imageDim, scale=scale)
    Random.seed!(seed)
    test_inputs = get_natural_scenes(nTestPatterns, s, imageDim, scale=scale)

    test_times = collect(0:div(nSteps, 10):nSteps)

    return main("scenes_rate_scan_random", plotflag, s, nSteps, inputs, test_inputs, test_times, 
                basefolder="/data.nst/fabmik/dendritic_balance/", startingweights=xz_weights)
end

plotflag = true
id = parse(Int,ARGS[2])

folder = ARGS[1]
subfolders = readdir(folder)
file = folder * subfolders[id] * "/net.bson"

dict = BSON.load(file)
net = dict[:net]

m = Int64(net.n_z)
rate = net.log.settings["rho"]
xz_weights = copy(net.xz_weights)
xz_weights = map(x->max(0,x), xz_weights)
shuffle!(xz_weights)
main_scenes(plotflag, copy(standardSettings), m, rate, xz_weights)

