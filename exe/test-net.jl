include("../src/main.jl")

using BSON
using Random

@assert (length(ARGS) > 0) "Please provide the name of the net you want to run."

file = ARGS[1]
runfolder = join(split(file,"/")[1:end-1],"/")

dict = BSON.load("$(file)")
net = dict[:net]


println("Setting up...")
nTestPatterns = 3000
imageDim = 12
seed = 1234
Random.seed!(seed)
test_inputs = get_natural_scenes(nTestPatterns, net.log.settings, imageDim)


println("Running $(file)")
net.log.t += 1
net.log.settings["reparametrizeBias"] = false
take_snapshot(net, net.log, test_inputs, net.log.settings)

save_loc = "$(runfolder)/test/"
mkpath(save_loc)
write_status(save_loc, net, net.log)
