import matplotlib.pyplot as plt
import h5py
import numpy as np
import sys, os, shutil
from plot_utils import *
from scipy.stats import pearsonr, ttest_ind


def permutation_test(x, y, f, n):
    obs = f(x) - f(y)
    samples = np.zeros(n)
    alls = np.hstack((x,y))
    inds = np.hstack((np.repeat([True], len(x)), np.repeat([False], len(y))))
    for i in range(n):
        np.random.shuffle(inds)
        xs = alls[inds]
        ys = alls[np.logical_not(inds)]
        samples[i] = f(xs) - f(ys)
    p = np.mean(np.abs(samples) >= np.abs(obs))
    print(np.abs(samples))
    return (samples, obs, p)



def main():
    ARGS = sys.argv
    if (len(ARGS) <= 2):
        print("Please provide two folders as argument (normal net, random weights net). Also, make sure you ran exe/scenes_test_balance.jl on the second net beforehand.")
        return

    filename1 = ARGS[1] + "/log.h5"
    filename2 = ARGS[2] + "/log.h5"

    print("Plotting " + filename1)
    dic = h5py.File(filename1, 'r')
    dic_rand = h5py.File(filename2, 'r')
    folder = ARGS[1] + "/python_plots/"
    #shutil.rmtree(folder, ignore_errors=True)

    try:
        os.mkdir(folder)
    except:
        pass

    plot_net(dic, dic_rand, folder)

def plot_net(dic, dic_rand, folder):
    # plot temp
    temp = dic[u'temp']
    meta = temp.attrs

    # how to get elements of hdf5
    snapshots = [dic[k] for k in sorted(list(dic.keys())) if 'snapshot' in k]
    times = [snapshot.get('t')[()] for snapshot in snapshots]
    snapshot_good = snapshots[times.index(np.max(times))]
    
    snapshots = [dic_rand[k] for k in sorted(list(dic_rand.keys())) if 'snapshot' in k]
    times = [snapshot.get('t')[()] for snapshot in snapshots]
    snapshot_rand_all = snapshots[times.index(-1)]

    snapshots = [snapshot_good, snapshot_rand_all]

    def get_condition_name(i_cond):
        names = ["good", "bad_everything"]
        return names[i_cond]
     
    for i_cond in range(len(snapshots)):
        snapshot = snapshots[i_cond]

        N = 2 # sliding window average
        i = 17 #4 # index of dendrite, find one where all F[i_n,i] are not small
        i_n = 3 # index of neuron
        offset = 10000
        t_start = 5000 + offset
        t_end = 30000 + offset

        t = snapshot.get('t')[()]
        dt = meta["dt"]
        interval = meta["tempLogSampleInterval"]
        # julia and python have reversed dimensions
        F = snapshot.get('xz_weights')[()][:,i_n]
        W = snapshot.get('zz_weights')[()][:,i_n]
        D = snapshot.get('decoder_weights')[()][:,i]
        x = snapshot.get('x_outputs')[()]
        z = snapshot.get('z_outputs')[()]
        rec = snapshot.get('reconstructions')[()]

        D_shuffled = np.copy(snapshot.get('decoder_weights')[()])
        np.random.shuffle(D_shuffled)
        
        # sliding window average to smooth out kinks a little
        x_avg = np.apply_along_axis(lambda m: np.convolve(m, np.ones((N,))/N, mode='same'), axis=0, arr=x)
        z_avg = np.apply_along_axis(lambda m: np.convolve(m, np.ones((N,))/N, mode='same'), axis=0, arr=z)

        print(t)

        ### Dendrite

        u_ex = F[i] * x[i,:] # multiply F with x and keep time axis
        if i_cond == 1: # if weights are not learned we take the shuffled inhibition
            u_inh = -F[i] * np.einsum('ji,jk->ik',D_shuffled, z)[i_n,:]
        else:
            u_inh = -F[i] * rec[i,:]
        

        u_ex_avg = np.apply_along_axis(lambda m: np.convolve(m, np.ones((N,))/N, mode='same'), axis=0, arr=u_ex)
        u_inh_avg = np.apply_along_axis(lambda m: np.convolve(m, np.ones((N,))/N, mode='same'), axis=0, arr=u_inh)

        fig, axs = plt.subplots(3, 1, figsize=(0.5*4,0.5*4))

        plt.suptitle(r'$r={0:.3}$'.format(pearsonr(u_ex, u_inh)[0]))
        #plt.suptitle(r'$\frac{\sigma^2_{u}}{\sigma^2_{ex}}='+r'{0:.3}$'.format(np.var(u_ex-u_inh)/np.var(u_ex)))

        ascale = 0.4 * np.max(u_ex_avg[t_start//interval:t_end//interval])
        axs[0].axis('off') 
        axs[1].axis('off') 
        axs[2].axis('off') 
        axs[0].plot(u_ex_avg[t_start//interval:t_end//interval], color='lightcoral')
        axs[0].set_ylim((-0.5*ascale,7.5*ascale))
        axs[1].plot(u_ex_avg[t_start//interval:t_end//interval]+u_inh_avg[t_start//interval:t_end//interval], color='gray')
        axs[1].set_ylim((-4*ascale,4*ascale))
        axs[2].plot(u_inh_avg[t_start//interval:t_end//interval], color='cornflowerblue')
        axs[2].set_ylim((-7.5*ascale,0.5*ascale))
        plt.annotate(
            '', xy=(-1500/interval, -5*ascale), xycoords='data',
            xytext=(-1500/interval, -1*ascale), textcoords='data',
            arrowprops={'arrowstyle': '|-|', 'mutation_scale': 1, 'mutation_aspect': 1})
        plt.annotate('{} '.format(4*ascale), xy=(-1600/interval, -3*ascale), xytext=(-1600/interval, -3*ascale), horizontalalignment='right', verticalalignment='center')
        plt.annotate(
            '', xy=(0, -6*ascale), xycoords='data',
            xytext=(10000/interval, -6*ascale), textcoords='data',
            arrowprops={'arrowstyle': '|-|', 'mutation_scale': 1, 'mutation_aspect': 1})
        plt.annotate('{} s'.format(10000*dt/1000), xy=(5000/interval, -7*ascale), xytext=(5000/interval, -7*ascale), horizontalalignment='center', verticalalignment='top')
        plt.savefig(folder + "balance_dendrite_" + get_condition_name(i_cond) + ".svg")
        plt.close()

        ### Soma

        fig, axs = plt.subplots(3, 1, figsize=(0.5*4,0.5*4))
        
        axs[0].axis('off') 
        axs[1].axis('off') 
        axs[2].axis('off') 
        ex_avg = np.einsum('j,jk->k',F, x_avg)
        ex = np.einsum('j,jk->k',F, x)
        if t==-1 or t==-3:
            inh_avg = np.einsum('j,jk->k',W, z_avg)
            inh = np.einsum('j,jk->k',W, z)
        else:
            rec = snapshot.get('reconstructions')[()]
            rec_avg = np.apply_along_axis(lambda m: np.convolve(m, np.ones((N,))/N, mode='same'), axis=0, arr=rec)
            inh = np.einsum('j,jk->k',F, -rec)
            inh_avg = np.einsum('j,jk->k',F, -rec_avg)
        som_avg = ex_avg + inh_avg

        plt.suptitle(r'$r={0:.3}$'.format(pearsonr(ex, inh)[0]))
        #plt.suptitle(r'$\frac{\sigma^2_{u}}{\sigma^2_{ex}}='+r'{0:.3}$'.format(np.var(ex+inh)/np.var(ex)))

        ascale = 0.3 * np.max(ex_avg[t_start//interval:t_end//interval])
        axs[0].plot(ex_avg[t_start//interval:t_end//interval], color='lightcoral')
        axs[0].set_ylim((-ascale,7*ascale))
        axs[1].plot(som_avg[t_start//interval:t_end//interval], color='gray')
        axs[1].set_ylim((-4*ascale,4*ascale))
        axs[2].plot(inh_avg[t_start//interval:t_end//interval], color='cornflowerblue')
        axs[2].set_ylim((-7*ascale,ascale))
        plt.annotate(
            '', xy=(-1500/interval, -5*ascale), xycoords='data',
            xytext=(-1500/interval, -1*ascale), textcoords='data',
            arrowprops={'arrowstyle': '|-|', 'mutation_scale': 1, 'mutation_aspect': 1})
        plt.annotate('{} '.format(4*ascale), xy=(-1600/interval, -3*ascale), xytext=(-1600/interval, -3*ascale), horizontalalignment='right', verticalalignment='center')
        plt.annotate(
            '', xy=(0, -6*ascale), xycoords='data',
            xytext=(10000/interval, -6*ascale), textcoords='data',
            arrowprops={'arrowstyle': '|-|', 'mutation_scale': 1, 'mutation_aspect': 1})
        plt.annotate('{} s'.format(10000*dt/1000), xy=(5000/interval, -7*ascale), xytext=(5000/interval, -7*ascale), horizontalalignment='center', verticalalignment='top')
        plt.savefig(folder + "balance_soma_" + get_condition_name(i_cond) + ".svg")
        plt.close()

    # correlation plots

    maxDendrites = 30
    F = snapshots[0].get('xz_weights')[()]
    soma_correlations = np.zeros((2,np.size(F,1)))
    dendrite_correlations = np.zeros((2,np.size(F,1),min(maxDendrites,np.size(F,0))))

   
    for i_cond in range(len(snapshots)):
        snapshot = snapshots[i_cond]

        F = snapshot.get('xz_weights')[()]
        D = snapshot.get('decoder_weights')[()]
        x = snapshot.get('x_outputs')[()]
        z = snapshot.get('z_outputs')[()]
        if i_cond==1: # if weights are not learned we take the shuffled inhibition
            rec = np.einsum('ji,jk->ik',D_shuffled, z)
        else:
            rec = snapshot.get('reconstructions')[()]
        

        for i_n in range(np.size(F,1)):
            
            ### Soma
            ex = np.einsum('j,jk->k',F[:,i_n], x)
            inh = np.einsum('j,jk->k',F[:,i_n], -rec)
            soma_correlations[i_cond,i_n] = pearsonr(ex, inh)[0]
            
            ### Dendrite
            used = []
            i_d = np.random.randint(0,np.size(F,0))
            for num in range(min(maxDendrites,np.size(F,0))):
                while(F[i_d,i_n]==0 or i_d in used):
                    i_d = np.random.randint(0,np.size(F,0))
                used.append(i_d)
                u_ex = F[i_d,i_n] * x[i_d,:] # multiply F with x and keep time axis
                u_inh = - F[i_d,i_n] * rec[i_d,:]
                dendrite_correlations[i_cond,i_n,num] = pearsonr(u_ex, u_inh)[0]
                i_d = np.random.randint(0,np.size(F,0))

    dendrite_correlations = np.reshape(dendrite_correlations,(2,-1))

    fig, axs = plt.subplots(2, 1, figsize=(2,3.7))
    axs[0].boxplot(soma_correlations.T,bootstrap=500,showfliers=False)
    axs[0].xaxis.set_major_formatter(plt.NullFormatter())
    axs[0].set_xticks([])
    axs[0].set_ylim([-1.0,0.15])
    axs[0].set_ylabel("pearson correlation")
    axs[1].boxplot(dendrite_correlations.T,bootstrap=500,showfliers=False,
                    labels=["weights learned", "weights random"])
    plt.xticks(rotation=-45, ha='left')
    axs[1].set_ylim([-1.0,0.15])
    axs[1].set_ylabel("pearson correlation")
    plt.tight_layout()
    plt.savefig(folder + "boxplot.svg")
    plt.close()


    print("soma, bad everythin vs model")
    print(permutation_test(soma_correlations[0,:], soma_correlations[1,:], np.median, 10000)[2])
    print(ttest_ind(soma_correlations[0,:], soma_correlations[1,:]))
    print("dendrite, bad everythin vs model")
    print(permutation_test(dendrite_correlations[0,:], dendrite_correlations[1,:], np.median, 10000)[2])
    print(ttest_ind(dendrite_correlations[0,:], dendrite_correlations[1,:]))



main()
