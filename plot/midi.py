import h5py
import sys, os, shutil

import numpy as np
from mido import Message, MidiFile, MidiTrack
import matplotlib.pyplot as plt

""" Converts spiketimes of a net to a midi file. Every neuron gets assigned a different pitch.
    spikes (2D np.array): [#neurons,#spikes], contains ordered spike times of neurons, -1 as placeholder.
    
    There are only 128 notes, so basenote and notespacing have to be adjusted accordingly to the number of neurons.
    If there is an error (negative Int), try to reduce the notelength."""
def spikes_to_midi_and_eventplot(spikes,
    speed = 0.05,
    tmin = 0,
    tmax = 6000,
    basenote = 80,
    notespacing = -1,
    notelength = 1.0,
    instrument = 12):

    data = []
    for n in range(spikes.shape[1]):
        neu = []
        for t in range(spikes.shape[0]):
            if spikes[t,n]>=tmin and spikes[t,n]<tmax:
                neu.append(spikes[t,n]) 
        data.append(neu)

    mid = MidiFile(type=1)

    for i in range(len(data)):
        track = MidiTrack()
        mid.tracks.append(track)
        track.append(Message('program_change', program=instrument, time=0))
        neu = data[i]
        t = 0
        for st in neu:
            ts = int(st/speed) - t - int(notelength/speed)
            t = int(st/speed)
            track.append(Message('note_on', note=int(basenote+notespacing*i), velocity=64, time=ts))
            track.append(Message('note_off', note=int(basenote+notespacing*i), velocity=127, time=int(notelength/speed)))
            
    mid.save('new_song.mid')
    
    fig = plt.figure()
    
    plt.eventplot(data, color='cornflowerblue')
    plt.savefig("test.svg")

def main():
    ARGS = sys.argv
    if (len(ARGS) <= 1):
        print("Please provide one file as argument")
        return
   
    hfile = ARGS[1]
    

    print("Plotting...")
    dic = h5py.File(hfile, 'r')
    
    spikes = dic['spikes'].get('spikes')[()]
    spikes_to_midi_and_eventplot(spikes)
    
main()



