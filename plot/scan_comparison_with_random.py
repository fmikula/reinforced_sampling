import matplotlib.pyplot as plt
import h5py
import numpy as np
import sys, os, shutil
from plot_utils import *


def main():
    ARGS = sys.argv
    if (len(ARGS) <= 3):
        print("Please provide two folders as argument (1. dendritic logs, 2. somatic logs, 3. random logs).")
        return
    
    N = 1  # rates, vars
    M = 9 # neurons, dt
    skip = 0
    folders_dendritic = [ARGS[1] + s for s in os.listdir(ARGS[1])]
    folders_somatic = [ARGS[2] + s for s in os.listdir(ARGS[2])]
    folders_random = [ARGS[3] + s for s in os.listdir(ARGS[3])]
    
    folders_dendritic = sorted(folders_dendritic)[skip:skip+N*M]
    folders_somatic = sorted(folders_somatic)[skip:skip+N*M]
    folders_random = sorted(folders_random)[skip:skip+N*M]
    file = "/log.h5"

    print("Plotting...")

    losses_dendritic = {}
    losses_somatic = {}
    losses_random = {}
    weights_dendritic = {}
    weights_somatic = {}
    weights_random = {}
    ts_dendritic = {}
    ts_somatic = {}
    ts_random = {}
    for folder in folders_dendritic:
        dic = h5py.File(folder + file, 'r')
        temp = dic[u'temp']
        meta = temp.attrs
        
        key = (round(meta["n_z"] * meta["rho"] ,1), meta["rho"], meta["n_z"])
        losses_dendritic[key] = temp["test_decoder_loss"]

        snapshots = [dic[k] for k in sorted(dic.keys()) if 'snapshot' in k]
        weights_dendritic[key] = snapshots[-1].get('xz_weights')[()]

        ts_dendritic[key] = temp["t"]

    for folder in folders_somatic:
        dic = h5py.File(folder + file, 'r')
        temp = dic[u'temp']
        meta = temp.attrs
        
        key = (round(meta["n_z"] * meta["rho"],1), meta["rho"], meta["n_z"])
        losses_somatic[key] = temp["test_decoder_loss"]

        snapshots = [dic[k] for k in sorted(dic.keys()) if 'snapshot' in k]
        weights_somatic[key] = snapshots[-1].get('xz_weights')[()]

        ts_somatic[key] = temp["t"]
        
    for folder in folders_random:
        dic = h5py.File(folder + file, 'r')
        temp = dic[u'temp']
        meta = temp.attrs
        
        key = (round(meta["n_z"] * meta["rho"],1), meta["rho"], meta["n_z"])
        losses_random[key] = temp["test_decoder_loss"]

        snapshots = [dic[k] for k in sorted(dic.keys()) if 'snapshot' in k]
        weights_random[key] = snapshots[-1].get('xz_weights')[()]

        ts_random[key] = temp["t"]
    
    # compare endpoints of learning
    fig, axs = plt.subplots(figsize=(0.6*4,0.6*3))
    loss_d = []
    loss_o = []
    loss_r = []
    ms = []
    for key in sorted(list(losses_dendritic.keys())):
        ms.append(key[2])
        loss_o.append(np.min(losses_dendritic[key]))
        loss_d.append(np.min(losses_somatic[key]))
        loss_r.append(losses_random[key][-1])
        print(losses_somatic[key][()])
        print(losses_random[key][()])

    plt.plot(ms, loss_d, marker='o', markersize=3, color='orange', label="SB")
    plt.plot(ms, loss_o, marker='o', markersize=3, color='darkolivegreen', label="DB")
    plt.plot(ms, loss_r, marker='o', markersize=1, color='gray', label="baseline")
    plt.xlabel("# z-neurons")
    plt.ylabel("decoder loss")
    #plt.xscale("log")
    #plt.legend()


    poprate = [k for k in losses_dendritic.keys()][0][0]
    print(poprate)
    def m2rho(m):
        return poprate / m * 1000

    def rho2m(rho):
        return poprate / rho / 1000

    secax = axs.secondary_xaxis('top', functions=(m2rho, rho2m))
    secax.set_xlabel('single neuron rate [Hz]')

    from matplotlib.ticker import ScalarFormatter, NullFormatter
    for axis in [axs.xaxis]:
        axis.set_major_formatter(ScalarFormatter())
        axis.set_minor_formatter(NullFormatter())
    axs.set_xticks([50, 100, 200])
    secax.set_xticks([20,10,5])

    plt.tight_layout()
    plt.savefig("../../plots/scancompare_loss_poprate_compact_random.svg")


main()

