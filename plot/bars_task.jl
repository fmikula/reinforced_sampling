using PyPlot

function separate_subimages(X::Array{Float64, 2}, sub_length::Int, border_width::Int)
    bw = border_width
    splits = split_images(X, sub_length)
    s = size(splits)
    X_new = zeros(s[1], s[2]+2*bw, s[3]+2*bw)
    X_new .= minimum(X)
    X_new[:,bw+1:end-bw,bw+1:end-bw] = splits
    return stitch_images(X_new)
end

""" Splits images into square tiles of a given side-length"""
function split_images(X::Array{Float64, 3}, split_length::Int)
    n = size(X,1)
    image_slX = size(X,2)
    image_slY = size(X,3)

    sl = split_length
    dim = sl^2
    splitsX = div(image_slX, sl)
    splitsY = div(image_slY, sl)
    temp = zeros(n * splitsX * splitsY, dim)

    i = 0
    for ind_im in 1:n
        arr = X[ind_im, :, :]
        for sx in 1:splitsX
            for sy in 1:splitsY
                i += 1
                cut = arr[sl*(sx-1)+1:sl*sx, sl*(sy-1)+1:sl*sy]
                temp[i,:] = reshape(cut, :)
            end
        end
    end
    return temp
end

function split_images(X::Array{Float64, 2}, split_length::Int)
    image_slX = size(X,1)
    image_slY = size(X,2)

    sl = split_length
    splitsX = div(image_slX, sl)
    splitsY = div(image_slY, sl)
    temp = zeros(splitsX * splitsY, sl, sl)

    i = 0
    for sx in 1:splitsX
        for sy in 1:splitsY
            i += 1
            cut = X[sl*(sx-1)+1:sl*sx, sl*(sy-1)+1:sl*sy]
            temp[i,:,:] = cut
        end
    end
    return temp
end

""" Takes the tiles of n (image_dim,image_dim) images and stiches them together"""
function stitch_images(X::Array{Float64,2}, n::Int, image_dim::Int, split_length::Int)
    m = size(X,1)
    cut_dim = size(X,2)

    sl = split_length
    splits = div(image_dim, cut_dim)
    temp = zeros(n, image_dim, image_dim)

    i = 0
    for ind_im in 1:n
        for sx in 1:splits
            for sy in 1:splits
                i += 1
                cut = reshape(X[i, :], (sl, sl))
                temp[ind_im, sl*(sx-1)+1:sl*sx, sl*(sy-1)+1:sl*sy] = cut
            end
        end
    end
    return temp
end

function stitch_images(X::Array{Float64,3})
    m = Int(sqrt(size(X,1)))
    sl = size(X,2)

    image_dim = m * sl
    temp = zeros(image_dim, image_dim)

    i = 0
    for sx in 1:m
        for sy in 1:m
            i += 1
            cut = X[i, :, :]
            temp[sl*(sx-1)+1:sl*sx, sl*(sy-1)+1:sl*sy] = cut
        end
    end
    return temp
end


function plot_patterns(patterns::Array{Float64, 2})
    folder = "../../plots/"

    nPatterns = min(64,size(patterns)[1])
    n = size(patterns)[2]
    nImg = n
    sidelength = Int(sqrt(nImg))
    nCols = Int(ceil(sqrt(nPatterns)))

    img = zeros(nCols*sidelength,nCols*sidelength)
    for i in 1:nPatterns
        pattern = patterns[i,:]
        col = ((i-1)%nCols)
        row = div(i-1,nCols)
        img[col*sidelength+1:(col+1)*sidelength, row*sidelength+1:(row+1)*sidelength] =
            reshape(pattern,sidelength,sidelength)
    end
    img = separate_subimages(img, sidelength, 1)
    PyPlot.clf()
    PyPlot.imshow(img',cmap="Greys")
    ax = PyPlot.gca()
    ax.axis("off")
    PyPlot.colorbar()
    PyPlot.savefig(folder * "patterns.svg")
    PyPlot.close_figs()
end

function create_stripe_inputs(nPatterns::Int, corr::Float64, noise::Float64)
    n = 8^2
    sidelength = Int(sqrt(n))
    @assert (n == sidelength^2) "n has to be square number!"

    strengthOn = 1
    strengthOff = 0

    input = ones(nPatterns, n) * strengthOff
    bars = zeros(Int, nPatterns, sidelength*2)
    randos1 = rand(1:2,nPatterns) # hor/vert
    randos2 = rand(1:sidelength,nPatterns) # #bar
    randos3 = rand(nPatterns) # for correlation between har/vert bar
    randos4 = rand(1:2,nPatterns)
    randos5 = rand(1:sidelength,nPatterns)
    for i in 1:nPatterns
        square = zeros(sidelength,sidelength)
        if (randos1[i] == 1)
            square[:, randos2[i]] .= strengthOn
            bars[i,randos2[i]] = 1
        else
            square[randos2[i], :] .= strengthOn
            bars[i,sidelength-1+randos2[i]] = 1
        end
        if (randos3[i] <= corr) # same bars are shown
            square[:, randos2[i]] .= strengthOn
            square[randos2[i], :] .= strengthOn
            bars[i,randos2[i]] = 1
            bars[i,sidelength-1+randos2[i]] = 1
        else
            if (randos4[i] == 1)
                square[:, randos5[i]] .= strengthOn
                bars[i,randos5[i]] = 1
            else
                square[randos5[i], :] .= strengthOn
                bars[i,sidelength-1+randos5[i]] = 1
            end
        end

        input[i,:] .= reshape(square,:)
    end
    return input + randn(nPatterns, n) * noise
end

function main()
    sidelength = 8
    step_corr = 0.1
    step_noise = 0.00 # 0.07
    input = zeros(sidelength^2, 8^2)
    k = 1
    for i in 0:(sidelength-1)
        for j in 0:(sidelength-1)
            input[k,:] = reshape(create_stripe_inputs(1, step_corr*i, step_noise*j),:)
            k=k+1
        end
    end

    plot_patterns(input)
end

main()










