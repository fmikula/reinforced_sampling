

function calc_var(net::Net)::Float64
    x = net.x_outputs
    rec = net.reconstruction

    M = x - rec
    return M.^2
end

function calc_var(net::Net, x_input::Array{Float64,1})::Float64
    x = x_input
    rec = net.reconstruction

    M = x - rec
    return M.^2
end

function log_likelihood(net::Net)::Float64
    var = net.delta_u
    x = net.x_outputs
    rec = net.reconstruction

    post = log_multivariate_gaussian(x, rec, var^(0.5))
    return post 
end

function log_likelihood(net::Net, x_input::Array{Float64,1}, var::Array{Float64,1})::Float64
    x = x_input
    rec = net.reconstruction

    post = log_multivariate_gaussian(x, rec, var)
    return post
end

function log_decoder_likelihood(net::Net)::Float64
    x = net.x_outputs
    sigma = net.log.decoder.sigma
    rec = net.reconstruction

    l = log_multivariate_gaussian(x, rec, sigma)
    return l
end

function log_decoder_free_energy(net::Net)::Float64
    x = net.x_outputs
    z = net.z_outputs
    z_s = net.z_spikes
    W = net.log.decoder.W
    sigma = net.log.decoder.sigma
    bias = net.log.decoder.biases
    rec = net.reconstruction

    p = log_decoder_free_energy_helper

    dyn = sum([log(n.prob) for n in net.z_neurons])
    prior = sum([log(p(bias[i] + W[i,:]' * z, Float64(z_s[i]))) for i in 1:net.n_z])
    post = log_multivariate_gaussian(x, rec, sigma)
    return post + prior - dyn
end

function log_decoder_free_energy_helper(x::Float64,s::Float64)::Float64
    return s * p_spike(x) + (1.0 - s) * (1.0 - p_spike(x))
end
