function calc_z_input(net::Net, s::Dict{String,Any})::Array{Float64,1}
    F = net.xz_weights
    W = net.zz_weights
    bias = net.z_biases
    delta_u = net.delta_u
    x = net.x_outputs
    z = net.z_outputs

    inputs = (F * x .+ W * z .+ bias) ./ delta_u
    return inputs
end

function create_xz_connections(s::Dict{String,Any})::Array{Float64,2}
    n_x = s["n_x"]::Int
    n_z = s["n_z"]::Int
    wVar = s["weightVariance"]::Float64
    wMean = s["weightMean"]::Float64

    xz_weights = randn(n_z, n_x) * sqrt(wVar) .+ wMean
    xz_weights = map(x -> max(0, x), xz_weights)

    return xz_weights
end
