
# struct is in log.jl

function create_decoder(s::Dict{String,Any})
    rho = s["rho"]::Float64
    dt = s["dt"]::Float64
    sigmas = s["initialSigma"] .* ones(s["n_x"])
    biases = ones(s["n_z"]) .* logit(rho * dt)
    dec = Decoder(zeros(s["n_x"],s["n_z"]), zeros(s["n_z"],s["n_z"]),
                   sigmas, biases)
    return dec
end

function update_decoder(net::Net, s::Dict{String,Any})
    eta = s["learningRateDecoder"]::Float64 * s["dt"]::Float64 
    eta_W = s["learningRateDecoderRecurrent"]::Float64 * s["dt"]::Float64 
    alpha = s["learningRateRewardMoments"]::Float64 * s["dt"]::Float64
    minSigma = s["initialSigma"]::Float64
    D = net.log.decoder.D
    W = net.log.decoder.W
    sigma = net.log.decoder.sigma
    bias = net.log.decoder.biases
    x = net.x_outputs
    z = net.z_outputs
    
    z_avg = net.memory["z_average"]::Array{Float64,1}
    z_avg .= (1 - alpha) .* z_avg + alpha .* z
    # normalize z to fix learning speed
    # but allow only speedup until <z> < 0.7
    z_normalized = z ./ map(x->max(0.7,x), z_avg)

    err = x .- D * z
    dD = err * z_normalized'
    net.log.decoder.D += eta * dD
    
    n_x = net.n_x
    @inbounds for i in 1:n_x
        dsigma = (err[i]^2 - sigma[i]^2) * sigma[i]^(-1)
        sigma[i] += eta * dsigma
        sigma[i] = max(sigma[i], minSigma)
    end
  
    z_s = net.z_spikes
    prior = [p_spike(bias[i] + W[i,:]' * z) for i in 1:net.n_z]
    dW = (z_s - prior) * z_normalized'
    net.log.decoder.W += eta_W * dW
    
end

function reconstruct_input(net::Net, s::Dict{String,Any})::Array{Float64,1}
    z = net.z_outputs
    D = net.log.decoder.D

    return D * z
end

""" Calculates decoder loss in respect to the current network input"""
function calc_decoder_loss(net::Net)::Float64
    D = net.log.decoder.D
    -x = net.x_outputs
    z = net.z_outputs
    rec = net.reconstruction

    err = x - D * z
    return 0.5 / net.n_x * err' * err
end

""" Calculates decoder loss in respect to input x"""
function calc_decoder_loss(net::Net, x::Array{Float64,1})::Float64
    D = net.log.decoder.D
    z = net.z_outputs
    rec = net.reconstruction

    err = x - D * z
    return 0.5 / net.n_x * err' * err
end
