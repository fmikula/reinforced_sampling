using Statistics: mean, var
using Dates
""" The temp log can be used to save additional quantities.
They will be plotted automatically in the end. The 'runningLog'
is used to average over time until the next logging point is reached.

To log something:
1. create desired variable in runningLog & temp log in *setup_temp_log*
2. update runningLog every timestep in *update_running_log*
3. save results from averaging to the temp log in *log_temp_log*"""

function setup_temp_log(net::Net, nSteps::Int, interval::Int)::Dict{String, Any}
    runningLog = Dict{String, Any}()

    # time of log
    net.log.temp["t"] = zeros(div(nSteps, interval))

    # variance learned by the net
    net.log.temp["delta_u"] = zeros(div(nSteps, interval))
    runningLog["delta_u"] = 0.0

    # mean firing rate
    net.log.temp["mean_firing_rate"] = zeros(div(nSteps, interval))
    runningLog["mean_firing_rate"] = 0.0
    
    net.log.temp["firing_rates"] = zeros(div(nSteps, interval), net.n_z)
    runningLog["firing_rates"] = zeros(net.n_z)

    # mean z bias
    net.log.temp["mean_z_bias"] = zeros(div(nSteps, interval))
    runningLog["mean_z_bias"] = 0.0

    # mean xz weights
    net.log.temp["mean_xz_weights"] = zeros(div(nSteps, interval))
    runningLog["mean_xz_weights"] = 0.0

    # var xz weights
    net.log.temp["var_xz_weights"] = zeros(div(nSteps, interval))
    runningLog["var_xz_weights"] = 0.0

    # membrane
    net.log.temp["z_inputs"] = zeros(div(nSteps, interval), net.n_z)
    runningLog["z_inputs"] = zeros(net.n_z)

    # decoder var
    net.log.temp["decoder_var"] = zeros(div(nSteps, interval), net.n_x)
    runningLog["decoder_var"] = zeros(net.n_x)

    # test measures
    net.log.temp["test_decoder_loss"] = zeros(div(nSteps, interval))
    net.log.temp["test_decoder_likelihood"] = zeros(div(nSteps, interval), 2)
    
    # reward
    net.log.temp["average_reward"] = zeros(div(nSteps, interval))
    net.log.temp["variance_reward"] = zeros(div(nSteps, interval))
    
    # eligibility
    net.log.temp["average_eligibility"] = zeros(div(nSteps, interval), net.n_z)
    runningLog["average_eligibility"] = zeros(net.n_z)

    return runningLog
end

function update_running_log(net::Net, runningLog::Dict{String,Any},
    x_input::Array{Float64,1}, interval::Int, numSamples::Int, s::Dict{String,Any})

    t = net.log.t
    k = div(t - 1, interval) + 1

    runningLog["delta_u"] += net.delta_u / numSamples
    runningLog["mean_z_bias"] += mean(net.z_biases) / numSamples
    runningLog["mean_xz_weights"] += mean(net.xz_weights) / numSamples
    runningLog["var_xz_weights"] += var(net.xz_weights) / numSamples
    runningLog["z_inputs"] += net.z_input ./ numSamples
    runningLog["decoder_var"] += net.log.decoder.sigma.^2 ./ numSamples
    runningLog["average_eligibility"] += net.memory["T_eligibility"] ./ numSamples
end

function log_temp_log(net::Net, runningLog::Dict{String,Any},
    inputs::Array{Float64,2}, interval::Int, s::Dict{String,Any})

    t = net.log.t
    k = div(t - 1, interval) + 1
    dt = net.log.settings["dt"]
    temp = net.log.temp

    test_performance(net, inputs, interval, s)

    temp["t"][k] = t

    temp["delta_u"][k] = runningLog["delta_u"]
    runningLog["delta_u"] = 0.0

    temp["mean_firing_rate"][k] = runningLog["mean_firing_rate"] / (0.001 * dt)
    runningLog["mean_firing_rate"] = 0.0

    temp["firing_rates"][k,:] = runningLog["firing_rates"] / (0.001 * dt)
    runningLog["firing_rates"] = zeros(net.n_z)

    temp["mean_z_bias"][k] = runningLog["mean_z_bias"]
    runningLog["mean_z_bias"] = 0.0

    temp["mean_xz_weights"][k] = runningLog["mean_xz_weights"]
    runningLog["mean_xz_weights"] = 0.0

    temp["var_xz_weights"][k] = runningLog["var_xz_weights"]
    runningLog["var_xz_weights"] = 0.0

    temp["z_inputs"][k, :] = runningLog["z_inputs"]
    runningLog["z_inputs"] = zeros(net.n_z)

    temp["decoder_var"][k, :] = runningLog["decoder_var"]
    runningLog["decoder_var"] = zeros(net.n_x)
    
    temp["average_reward"][k] = net.memory["average_reward"]
    temp["variance_reward"][k] = net.memory["variance_reward"]
    
    temp["average_eligibility"][k, :] = runningLog["average_eligibility"]
    runningLog["average_eligibility"] = zeros(net.n_z)
end

""" Tests the performance on a training data-set."""
function test_performance(net::Net, inputs::Array{Float64,2}, interval::Int, s::Dict{String,Any})
    l = s["presentationLength"]::Int

    nSteps = size(inputs, 1) * l
    k = div(net.log.t - 1, interval) + 1
    temp = net.log.temp

    for i in 1:nSteps
        x = fade_images(inputs,i,s)
        step_net(net, x, s, update=false)

        temp["test_decoder_loss"][k] += calc_decoder_loss(net, x) / nSteps
        temp["test_decoder_likelihood"][k,1] += log_decoder_likelihood(net) / nSteps
        temp["test_decoder_likelihood"][k,2] += log_decoder_free_energy(net) / nSteps
    end
end
