
function update_eligibility(net::Net, s::Dict{String, Any})
    lmbd = s["lambdaEligibility"]::Float64 * s["dt"]::Float64
    u = net.z_input
    ps = map(p_spike, u) 
    spike = net.z_spikes
    x = net.x_outputs
    z = net.z_outputs

    e_F = net.memory["F_eligibility"]::Array{Float64,2}
    e_W = net.memory["W_eligibility"]::Array{Float64,2}
    e_T = net.memory["T_eligibility"]::Array{Float64,1}

    e_F .*= lmbd
    e_F .+= (spike - ps) * x'
    e_W .*= lmbd
    e_W .+= (spike - ps) * z'
    e_T .*= lmbd
    e_T .+= (spike - ps) 
    #println(ps)
    #println(u)
end

function update_reward(net::Net, s::Dict{String, Any})
    alpha = s["learningRateRewardMoments"]::Float64 * s["dt"]::Float64
    bs = net.log.decoder.biases
    spikes = net.z_spikes
    ns = net.z_neurons 
    
    x = net.x_outputs
    z = net.z_outputs
    z_s = net.z_spikes
    sigma = net.log.decoder.sigma
    bias = net.log.decoder.biases
    W_d = net.log.decoder.W
    rec = net.reconstruction
    
    #p = log_decoder_free_energy_helper
    
    #dyn = [log(n.prob) for n in net.z_neurons]
    #prior = [log(p(bias[i] + W_d[i,:]' * z, Float64(z_s[i]))) for i in 1:net.n_z]
    #post = log_multivariate_gaussian(x, rec, sigma)
    
    avg = net.memory["average_reward"]::Float64
    sd = net.memory["variance_reward"]::Float64
    #r = post + sum(prior) - sum(dyn)
    r = log_decoder_free_energy(net) - 1
    net.memory["average_reward"] = (1 - alpha) * avg + alpha * r
    net.memory["variance_reward"] = (1 - alpha) * sd + alpha * (r - avg)^2
    net.memory["reward"] = (r - avg) / max(1, sqrt(sd))
    
    #avg = net.memory["F_average_reward"]::Array{Float64,2}
    #sd = net.memory["F_variance_reward"]::Array{Float64,2}
    #f = zeros(net.n_z, net.n_x)
    #for i in 1:net.n_x
    #    f[:,i] = log_gaussian(x[i], rec[i], sigma[i]) .+ prior .- dyn
    #    #f[:,i] = post .+ (prior .- dyn)
    #end
    #net.memory["F_average_reward"] = (1 - alpha) .* avg .+ alpha .* f
    #net.memory["F_variance_reward"] = (1 - alpha) .* sd .+ alpha .* (f .- avg).^2
    #net.memory["F_reward"] = (f - avg) ./ map(x -> max(1, x), map(sqrt,sd))
    
    #avg = net.memory["W_average_reward"]::Array{Float64,1}
    #sd = net.memory["W_variance_reward"]::Array{Float64,1}
    #f = - (net.delta_u * net.z_input - net.z_biases).^2
    #net.memory["W_average_reward"] = (1 - alpha) .* avg .+ alpha .* f
    #net.memory["W_variance_reward"] = (1 - alpha) .* sd .+ alpha .* (f .- avg).^2
    #net.memory["W_reward"] = (f - avg) ./ map(x -> max(1, x), map(sqrt,sd))
    
    #avg = net.memory["T_average_reward"]::Array{Float64,1}
    #sd = net.memory["T_variance_reward"]::Array{Float64,1}
    #f = post .+ prior .- dyn
    #net.memory["T_average_reward"] = (1 - alpha) .* avg .+ alpha .* f
    #net.memory["T_variance_reward"] = (1 - alpha) .* sd .+ alpha .* (f .- avg).^2
    #net.memory["T_reward"] = (f - avg) ./ map(x -> max(1, x), map(sqrt,sd))
    
    #println(avg)
    #println(sd)
    #println(net.memory["reward"])
    #println()
    
end

function update_net(net::Net, s::Dict{String, Any})
    learnedSigma = s["learnedSigma"]::Bool
    fixedFinalSigma = s["fixedFinalSigma"]::Bool

    update_xz_weights(net, s)
    update_zz_inhibitory_weights(net, s)
    update_z_biases(net, s)
end

function update_xz_weights(net::Net, s::Dict{String,Any})
    eta = s["learningRateFeedForward"]::Float64 * s["dt"]::Float64 
    e = net.memory["F_eligibility"]::Array{Float64,2}
    r = net.memory["reward"]::Float64
    
    dF = r .* e
    net.xz_weights += eta * dF
end

function update_zz_inhibitory_weights(net::Net, s::Dict{String,Any})
    eta = s["learningRateInhibitoryRecurrent"]::Float64 * s["dt"]::Float64 
    #z = net.z_outputs
    #u = net.z_input
    #du = net.delta_u
    #bias = net.z_biases
    
    #inp = du * u - bias
    #dW = - inp * z'
    
    e = net.memory["W_eligibility"]::Array{Float64,2}
    r = net.memory["reward"]::Float64
    
    dW = r .* e
    net.zz_weights += eta * dW
    #net.zz_weights = map(x -> min(0,x), net.zz_weights)
end

function update_z_biases(net::Net, s::Dict{String,Any})
    eta = s["learningRateBias"]::Float64 * s["dt"]::Float64 
    e = net.memory["T_eligibility"]::Array{Float64,1}
    r = net.memory["reward"]::Float64

    dT = r .* e
    net.z_biases += eta * dT
end

